<h1 align="center">
    <img alt="Launchbase" src="https://storage.googleapis.com/golden-wind/bootcamp-launchbase/logo.png" width="400px" />
</h1>

<h1 align="center">Projeto : Portfólio ( backend / nunjucks )</h1>
<p align="center"></p>

<p align="center">
  <a href="#sobre">Sobre o Projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#desafio2">Parte 2: Página de descrição</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#desafio3">Parte 3: Página de posts e iframe</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#images">Imagens do Desafio Finalizado</a>
</p>

<p align="center">
  <a href="https://rocketseat.com.br">
    <img alt="Made by Rocketseat" src="https://img.shields.io/badge/made%20by-Rocketseat-%23F8952D">
  </a>
  <a href="LICENSE" >
    <img alt="License" src="https://img.shields.io/badge/license-MIT-%23F8952D">
  </a>
</p>

<h2 id="sobre"> 🚀 Sobre o Projeto</h2>

Esse projeto de Portfólio foi desenvolvido utilizando nunjuks uma template engine rodando juntamente com um backend em nodejs, consultando dinâmicamente um objeto com dados existente dentro do projeto, para visualização das informações do portfólio e da página Sobre.

<h2 id="images"> 🖼️ Imagens do Desafio Finalizado</h2>

<div align="center">
 <img align="center" width="600px" alt="Made by Rocketseat" src="./images/sobre.png">
 </ br>
 <img align="center" width="600px" alt="Made by Rocketseat" src="./images/portfolio.png">
  </ br>
 <img align="center" width="600px" alt="Made by Rocketseat" src="./images/video.png">
</div>


### Tecnologias / Dependências
- JavaScript
- HTML
- CSS
- Nunjuks
- Nodemon
- Express


### 🎲 Rodando o Projeto

```bash
# Clone este repositório
# Acesse a pasta do projeto
# execute o comando
$ yarn start
ou
$ npm start

# O servidor inciará na porta:5000
```