const modalOverlay = document.querySelector('.modal-overlay');
const cards = document.querySelectorAll('.card');

for (let card of cards) {
  // no evento de click da classe de card eu addiciona a lista de classes a classe active
  card.addEventListener("click", function () {
    const videoId = card.getAttribute("id");
    modalOverlay.classList.add('active');

    modalOverlay.querySelector('iframe').src = `https://www.youtube.com/embed/${videoId}`
  })
}

// no evento de click da classe de close-modal eu excluo da lista de classes a classe active
// essa classe close-modal é onde fica o botão com o x para fechar o modal
document.querySelector('.close-modal').addEventListener("click", function () {
  // remove a classe active da lista de classes 
  modalOverlay.classList.remove('active')
  // passa o iframe como vazio para parar de passar o video
  modalOverlay.querySelector('iframe').src = "";

})

