// Criar um programa que calcula a média 
// das turmas de alunos e envia a mensagem de calculo da média
const alunosDaTurmaA = [
  {
    nome: "Bruno",
    nota: 9.8
  },
  {
    nome: "Ana",
    nota: 10
  },
  {
    nome: "Victor",
    nota: 2
  }
]

const alunosDaTurmaB = [
  {
    nome: "Laura",
    nota: 7
  },
  {
    nome: "Zé",
    nota: 5
  },
  {
    nome: "Daniela",
    nota: 2
  },
  {
    nome: "Mateus",
    nota: 4
  }
]

function calculaMedia(alunos) {
  let soma = 0;
  for (let i = 0; i < alunos.length; i++) {
    soma = soma + alunos[i].nota;
  }
  const media = soma / alunos.length;
  return media;
}

const media1 = calculaMedia(alunosDaTurmaA)
const media2 = calculaMedia(alunosDaTurmaB)

function enviaMensagem(media, turma) {
  if (media > 5) {
    console.log(`A media da turma ${turma} foi de ${media}. Parabéns`);
  } else {
    console.log(`A media da turma ${turma} é menor que 5`);
  }
}

enviaMensagem(media1, 'TurmaA')
enviaMensagem(media2, 'TurmaB')