/*
 OPERADORES DE COMPARAÇÃO

 >     Maior 
 <     Menor
 >=    Maior igual a 
 <=    Menor igual a 
 ==    Igual a
 ===   Igual e do mesmo tipo
 !=    Diferente de
 !==   Diferente, inclusive do tipo
 
*/

// DESAFIO 1 - OPERADORES DE COMPARAÇÃO
const idade = 17
// verificar se a pessoa é maior de 18 anos 
// se sim, deixar entrar, se não, bloquear a entrada  
if (idade >= 18) {
  console.log('Deixar Entrar')
} else {
  console.log('Bloquear Entrada')
}

// se a pessoa tiver 17 anos
// avisar para voltar quando fizer 18 anos 
if (idade === 17) {
  console.log('Volte qundo tiver 18');
}

/*
  OPERADORES LÓGICOS

  && "E" As duas condições devem ser verdadeiras
      para que aa condição final seja verdadeira.
  || "OU" Uma das condições deve ser verdadeira
      para que a condição final seja verdadeira
  !  "NÃO" Nega uma condição

*/

// DESAFIO 1 - OPERADORES LÓGICOS
const vidade = 18
// verificar se a pessoa é maior de 18 anos 
// se sim, deixar entrar, se não, bloquear a entrada 
// se a pessoa tiver 17 anos
// avisar para voltar quando fizer 18 anos  
if (!(vidade >= 18) || vidade === 17) {
  console.log('Bloquear Entrada')
} else {
  console.log('Deixar Entrar')
}

// dar bonificação de 500 reais 
// se vendedor possuir 100 ou menos pontos
// mas deve possuir mais de 50

/*
  OPERADORES ARITMÉTICOS

  *   Multiplicação
  /   Divisão
  %   Resto da Divisão
  +   Adição
  -   Subtração

*/

console.log(2 * 2) // 4
console.log(2 / 2) // 1
console.log(2 % 1.5) // 0.5
console.log(2 + 2) // 4
console.log(2 - 2) // 0
