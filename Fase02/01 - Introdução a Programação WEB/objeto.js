// Vetor de objeto de alunos
const alunos = [
  {
    nome: "Bruno",
    nota: 9.8
  },
  {
    nome: "Ana",
    nota: 10
  },
  {
    nome: "Victor",
    nota: 2
  }
]

const media = (alunos[0].nota + alunos[1].nota + alunos[2].nota) / 3;

console.log(media);
console.log(alunos);
// Mostra os alunos individualmente
console.log(alunos[0]);
console.log(alunos[1]);
console.log(alunos[2]);