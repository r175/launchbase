/*
 * Criar um programa que calcula a média das notas entre os alunos 
 * e envia mensagem do calculo da média
 */

const aluno01 = "Bruno"
const notAluno01 = 9.8

const aluno02 = 'Ana'
const notAluno02 = 10

const aluno03 = 'Victor'
const notAluno03 = 2

const media = (notAluno01 + notAluno02 + notAluno03) / 3;

// Se a média for maior que 5, parabenizar a turma
if (media > 5) {
  console.log(`A nota foi de ${media}. Parabéns`);
} else {
  console.log('A média é menor que 5');
}


